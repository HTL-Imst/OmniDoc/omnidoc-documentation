\pagebreak
\section{Einführung in SNMP} \label{sec:snmp-theorie}
\def \currentAuthor {Boris Müller}
Im folgendem Kapitel werden Basiselemente von SNMP\footnote{Simple Network Management Protocol} dargestellt und erklärt.
Das Protokoll SNMP verwendet die Ports 161 und 162 und wird in der RFC\footnote{Requests for Comments} 1157 und RFC 3413 definiert.
\subsection{Versionen von SNMP}

Die Auswirkungen der verschiedenen Versionen können in der Tabelle \ref{tab:snmp-versionen} nachgelesen werden. \citep[S. 2 von 12]{snmp-konfiguration_cisco_2018}.

\begin{table}[h!]
	\centering
	\small
	\setlength\tabcolsep{1pt}
	\caption{Beschreibung der Versionen von SNMP}
	\label{tab:snmp-versionen}
	\begin{tabular}{l|p{3cm}|p{3,4cm}|p{3cm}|p{5cm}}
		\rowcolor{TableGreen}
		\textbf{Model} & \textbf{Sicherheitslevel} & \textbf{Authentifizierung} & \textbf{Verschlüsselt} & \textbf{Ergebnis} \\ \hline
		SNMPv1 &
		noAuthNoPriv &
		Community String &
		nein &
		Für die Authentifizierung wird die Community verwendet.	\\ \hline
		SNMPv2c &
		noAuthNoPriv &
		Community String &
		nein &
		Für die Authentifizierung wird die Community verwendet.	\\ \hline
		SNMPv3 &
		noAuthNoPriv &
		Benutzername &
		nein &
		Für die Authentifizierung wird anstatt der Community ein Benutzername verwendet. 	\\ \hline
		SNMPv3 &
		authNoPriv &
		Benutzername und Passwort &
		nein &
		Benutzername und Passwort werden mittels MD5- oder SHA-Algorithmus verschlüsselt; beides wird für die Authentifizierung benötigt.	\\ \hline
		SNMPv3 &
		authPriv (benötigt Zugriff \
		auf das Software Image) &
		Benutzername und Passwort &
		Data Encryption Standard (DES) oder
		Advanced Encryption Standard (AES) &
		Benutzername und Passwort werden mittels MD5\footnote{Message-Digest Algorithm 5}- oder SHA\footnote{secure hash algorithm}-Algorithmus verschlüsselt.
		Ermöglicht es den Traffic mittels DES\footnote{Data Encryption Standard} 56-bit Standard, 3DES\footnote{Triple Data Encryption Standard} 168-Bit oder mit AES\footnote{Advanced Encryption Standard} 128-Bit, 192-Bit oder mit 256-Bit zu verschlüsseln. \\
	\end{tabular}
\end{table}

\pagebreak
\subsection{Begriffserklärungen für das Sicherheitslevel}
\begin{table}[h!]
	\centering
	\caption{Sicherheitslevel}
	\label{tab:sicherheitslevel}
	\begin{tabular}{p{4cm}|p{9cm}}
		\rowcolor{TableGreen}
		\textbf{Sicherheitslevel} & \textbf{Beschreibung} \\ \hline
		noAuthNoPriv &
		Authentifizierung nur durch die Community oder durch einen Benutzernamen ohne Passwort. Der Traffic wird nicht verschlüsselt. \\ \hline
		authNoPriv &
		Authentifizierung mittels Benutzername und Passwort, jedoch kein verschlüsselter Traffic. \\ \hline
		authPriv &
		Authentifizierung mittels Benutzername und Passwort.
		Der Traffic wird verschlüsselt. \\
	\end{tabular}
\end{table}

\subsection{SNMP Agent}
Ein SNMP Agent antwortet auf die Anfragen eines SNMP Managers.
Es wird unterschieden in:
\begin{itemize}
\item \textbf{Rückgabe einer MIB\footnote{Management Information Base} Variable.} \newline
Hier empfängt der Agent den Zeiger auf eine MIB Variable (Ressource) und gibt dessen Wert zurück.
\item \textbf{Setzen einer MIB Variable.} \newline
Hier ändert der Agent den Wert einer MIB Variable auf dessen Wert, der von der Zentrale neu definiert wurde.
\end{itemize}
Ein SNMP Agent kann auch sogenannte „Trap-Nachrichten“ oder auch Ereignisnachrichten an eine Zentrale schicken.
Bei bestimmten Veränderungen am System, schickt der Agent ein Info Paket an die Zentrale, die dann darauf reagieren kann. \citep[S. 3 von 12]{snmp-konfiguration_cisco_2018}
Beispiele:
\begin{itemize}
\item Ein Port bei einem Switch wird aktiv oder inaktiv.
\item Ein ganzes Modul funktioniert nicht mehr.
\item Große Veränderungen in der Spanning-Tree-Topology.
\item Wenn Authentifizierungsfehler auftreten.
\end{itemize}

\subsection{SNMP Community}
Version 1 und 2 von SNMP benötigen eine Community.
Die Community ist kein richtiges Sicherheitsfeature, da sie nur eine unverschlüsselte Zeichenkette ist.
Ein Angreifer müsste zuerst dieses einzelne Wort herausfinden, um Schaden anzurichten.
Die Community ermöglicht einen Zugriff auf die MIB Objekte.

Die Version 3 von SNMP benötigt keine Community.
Hier wird ein Benutzer verwendet, der mit einem Passwort hinterlegt werden kann. Benutzername und Passwort wird gehascht gespeichert.
Durch dies ist die Version 3 die einzige mit Sicherheitsfeatures. \citep[S. 3 von 12]{snmp-konfiguration_cisco_2018}

Ein Community String erfüllt eins von den beiden Attributen:
\begin{itemize}
\item \textbf{read-only (RO)} – ermöglicht es der Community alle Werte zu lesen, aber nicht zu verändern
\item \textbf{read-write (RW)} – ermöglicht es der Community alle Werte zu lesen und zu verändern
\end{itemize}

\subsection{SNMP Benachrichtigungen}
SNMP kann Benachrichtigungen an einen SNMP-Manager schicken, wenn bestimmte Ereignisse auftreten.
Mit dem Schlüsselwort \shellcmd{traps} kann definiert werden ob Ereignisse (= \textit{traps}) und / oder wichtige Informationen (= \textit{informs}) gesendet werden sollen.
Mit dem Kommando \shellcmd{snmp-server host} wird nur eines von beiden ausgewählt. \citep[S. 3 von 12]{snmp-konfiguration_cisco_2018}

Die Version SNMPv1 unterstützt diese Funktion der Benachrichtigung nicht.

Beim Verschicken von Ereignisinformationen bekommt der Sender keine Bestätigung vom Empfänger.
Beim Verschicken von wichtigen Informationen kann der Sender diese Informationen so oft senden, bis er eine Antwort vom Empfänger bekommt.
Die Anzahl, wie oft der Sender die Information sendet, kann eingestellt werden.

Ereignisse und wichtige Informationen benötigen Netzwerk Ressourcen.
Bei Netzwerken, bei denen der Traffic von SNMP ein Problem darstellt, sollte man \textit{traps} verwenden, da diese nur einmal gesendet werden.
Bei Informationen, die sehr wichtig sind, solltet man \textit{informs} verwenden, da diese so oft gesendet werden, bis sie der Empfänger bestätigt.

\subsection{Konfiguration einer SNMP Community}
Die Konfiguration der SNMP Community wird in der \cref{tab:konfiguration-community} gezeigt. \citep[S. 5 von 12]{snmp-konfiguration_cisco_2018}

\begin{table}[h!]
	\centering
	\caption{Konfiguration einer SNMP Community}
	\label{tab:konfiguration-community}
	\begin{tabular}{p{0.2cm}|p{5.8cm}|p{8cm}}
		\rowcolor{TableGreen}
	 	& \textbf{Befehlssatz} & \textbf{Eigenschaft} \\ \hline
		1. &
		\shellcmd{configure terminal} &
		In den Konfigurationsmodus wechseln
		\\ \hline
		2. &
		\shellcmd{snmp-server community string [view view-name] [ro \textbar rw]} \newline

		Beispiel: \newline
		\shellcmd{snmp-server community\newline CommunityName view ViewName ro}
		&
		Der Community Name dient zum Authentifizieren. Es können ein oder mehrere Communities erstellt werden. Die Länge des Namens ist egal.
		Mittels \textit{view} ermöglicht man der Community den Zugriff auf ein bestimmtes Objekt im MIB Baum.
		Zum Schluss kann der Wert \textit{ro} oder \textit{rw} angegeben werden. Dessen Bedeutung wird unter \textit{SNMP Community} erläutert.
		\\ \hline
		3. &
		\shellcmd{snmp-server view view-name oid-tree \{included \textbar excluded\}} \newline

		Beispiel: \newline
		\shellcmd{Snmp-server view ViewName iso \newline included}
		&
		Konfiguration einer \textit{view}.
		Mit diesem Befehl wird definiert auf welchen Bereich die View Zugriff hat.
		Zuerst wird der Name definiert. Danach kommt die \textit{MIB view family name}. Dies kann eine Nummer sein z.B. \shellcmd{.1.3.6.1} oder der Name eines Objektes z.B. \shellcmd{.internet} oder \shellcmd{.iso}.
		Wenn der \textit{MIB family name} dabei sein soll wird \textit{indcluded} ausgewählt ansonsten \textit{excluded}. \textit{excluded} wird verwendet, wenn der Bereich nicht für die Community vorgesehen ist.
		\end{tabular}
\end{table}

\pagebreak
\textbf{Beispiel:} Erstellen einer SNMP Community
\begin{figure}[h!]
	\centering
	\includegraphics[width=1.0\linewidth]{figures/snmpbash/erstellen_community.png}
	\caption[Erstellen einer SNMP Community]{Erstellen einer SNMP Community}
	\label{fig:konfiguration-community}
\end{figure}

\subsection{Management Information Base}
Die Management Information Base kurz MIB\footnote{Management Information Base} wird in der RFC 1155 definiert.
Durch den MIB-Baum werden die sogenannten Object Identifier, kurz OID erstellt.
Eine OID ist eine Zeichenkette z.B. \shellcmd{.1.3.6.1.5}.
Jede Zahl zeigt auf ein Objekt, welches einen Wert hält.
Umso kürzer die Zahlenkette ist, umso mehr Werte werden angesprochen.
Wenn man nun einen Wert möchte, z.B. den Hostnamen von einem Gerät dann benötigt es die komplette Zahlenkette, bis der Wert Hostname erreicht wurde.
Die Zeichenketten sind von Hersteller zu Hersteller unterschiedlich.
Bei Cisco ist es möglich von fast allen Geräten die MIBs zu bekommen und diese zu verwenden.
Dafür braucht es aber z.B. einen MIB Explorer.
Dieser kann die MIBs lesen und grafisch aufbereiten.
Nun ist es möglich eine bestimmte Zahlenkette für einen bestimmten Wert leichter zu suchen.

\pagebreak
\chapter{Bash Skripte}
In diesem Abschnitt werden die Skripte erklärt, welche mittels SNMPWALK die Netzwerkgeräte nach den vorgegebenen Parametern scannen sollen.
In folgender Grafik wird ersichtlich, wie der Ablauf der Skripte funktioniert.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.90\linewidth]{figures/snmpbash/Ablauf_der_Skripte.png}
	\caption[Ablauf der Skripte in Bash]{Ablauf der Skripte in Bash}
	\label{fig:ablauf-skripte}
\end{figure}

\pagebreak
\section{Funktionsweise der Skripte}
Zum Start wird das Skript \textit{snmpScan.bash} mit verschiedenen Parametern aufgerufen.
Dieses ist das zentrale Skript, welches alles bis zum Schluss leitet.
Am Anfang werden die zwei weiteren Skripte \textit{startEndeIPBereich.bash} und \textit{pingScan.bash} mit eingebunden.
Danach werden die übergebenen Parameter verarbeitet und den Skript Variablen übergeben.
Dies wurde mit einer for-Schleife und case-Anweisung realisiert.

\input{inputs/code/script/parameters.inp.tex}

Danach wird die Funktion \shellcmd{startEndeIPBereich() \$ip \$sm} im Skript \textit{startEndeIPBereich.bash} aufgerufen.

\input{inputs/code/script/hosts.inp.tex}

Dieses Skript berechnet, durch die Netz-ID und der Subnetmaske, den Bereich des IP-Netzes.

\input{inputs/code/script/netip.inp.tex}

Zurückgegeben wird der erste und letzte mögliche Host.
Grund dafür ist, dass herausgefunden werden muss wie viele Hosts zwischen dem ersten und dem letzten liegen.
Bei einer Anzahl von 254 Hosts dauert der Scan nach IP-Adressen in etwa 1 Minute und 22 Sekunden.
Damit z.B. 8094 Hosts nicht dementsprechend lange dauern, wird der Bereich in kleine Bereiche von 0 bis 255 unterteilt.
Es wird darauf geachtet, dass die Netz-ID und der Broadcast nicht gepingt werden.
Dieser kleine Bereich wird zum Scan in den Hintergrund verschoben, damit das Skript nicht warten muss.
Der letzte IP-Bereich wird nicht in den Hintergrund verschoben, da das Skript auf die letzten möglichen IP-Adressen warten muss, um danach weiter zu arbeiten.
\newline
Mit dem ersten und letzten Host wird das dritte Skript aufgerufen.
Dieses scannt den IP Bereich.
Bei diesem Scan werden erreichbare IP-Adressen ermittelt und gespeichert.
Der hier gezeigt Code gibt den letzten IP Bereich für den Scan frei.

\input{inputs/code/script/aufteilung.inp.tex}

Die abgespeicherten IP-Adressen werden nun für den SNMP Scan verwendet.
Durch diese Funktion werden die IP-Adressen ermittelt.

\input{inputs/code/script/scan.inp.tex}

Hier im Code wird das Wort \textit{nice} verwendet. Dies ermöglicht es die Priorität des Jobs, welche von bis -20 und +19 reicht, zu bestimmen.
Ziel ist es, dass bei vielen Hintergrundjobs die Hardware nicht überlastet werden soll.

\pagebreak
\subsection{Start der SNMP Scans}
Von den Geräten, welche SNMP unterstützen, werden die gewünschten Parameter erfasst.
Geräte, die kein SNMP unterstützen, werden nur mit IP-Adresse und Subnetzmaske abgespeichert.

\input{inputs/code/script/snmp-check.inp.tex}

Zum Schluss wird das Ergebnis in eine JSON Datei geschrieben und zur weiteren Verarbeitung verwendet.

\subsection{SNMPWALK}
Das Programm SNMPWALK steht unter der BSD\footnote{Berkeley Software Distribution}-Lizenz.
Somit kann dieses Programm für unser Projekt ohne Kosten oder Einschränkungen verwendet werden.
Wir verwenden es um SNMP Abfragen auf eine IP Adresse zu ermöglichen.
Je nach Version von SNMP sieht die Syntax anders aus.

\subsection{Ausführen mehrerer Scans}
Es ist möglich mehrere Scans gleichzeitig auszuführen.
Damit sich diese untereinander nicht stören, werden sogenannte Scan-IDs übergeben. Dies garantiert, dass sich die Scans nicht untereinander manipulieren. Auch die JSON-Rückgabe wird mit dieser ID bestückt.
Zum Schluss eines Scans werden unnötige Dateien, die als Zwischenspeicher dienten, automatisch wieder gelöscht.

\section{Technische Probleme}

\subsubsection{Hardwarelimitierung}
Die Scan-Jobs, für die IP-Adressen, werden in den Hintergrund verschoben.
Ab einer gewissen Anzahl an Jobs wird es für die Hardware zu viel und sie werden nicht mehr ausgeführt.
Der Programmablauf kommt zwar zum Schluss aber es werden nicht alle IP-Adressen überprüft.
Dieses Problem tritt ab einer Subnetzmaske von /8 bis /15 auf.
Durch die Größe des IP-Netzes, werden zu viele Hintergrundjobs erstellt.

\pagebreak
\subsubsection{Unterschiedliche OIDs}
Jeder Hersteller verwendet andere OIDs.
Die OIDs der Hersteller sind dabei auch von Gerät zu Gerät unterschiedlich.
Da wir zum Testen nur den Cisco Switch 2960 C hatten konnten wir vorerst nur OIDs für diesen Switch heraussuchen und in unsere Skripte einarbeiten.
Dieselben funktionierenden OIDs wurde auch an einem Cisco 1841 Router in der Schule getestet.
Ca. 50 Prozent der OIDs haben den richtigen Wert zurückgeliefert.
Bei abweichenden Nummern wurde entweder was falsches oder gar nichts angezeigt.

\subsubsection{VLAN und MAC-Adressen}
Bei der OID für die MAC-Adressen werden alle MAC-Adressen, inklusive die von den VLANs\footnote{Virtual Local Area Network}, zurückgegeben.
Wenn dem VLAN eine IP-Adresse zugewiesen wurde, wird automatisch eine MAC-Adresse für das VLAN erstellt.
Die Rückgabe der MAC-Adressen kann sich somit bei jedem Scan verändern.


\subsubsection{Verwendung von NMAP}
NMAP\footnote{Network Mapper} ist ein eigenes Programm, welches unter der GPLv3\footnote{General Public License Version 3} Lizenz steht.
Ports zu scannen, ist die wichtigste Funktion des Programmes.
Die Rückgabe sind offene und geschlossene Ports.
Um eine MAC-Adresse hinter der erreichbaren IP-Adresse herauszufinden, kann NMAP verwendet werden.



\section{Mögliche Lösungen}

\subsubsection{Hardwarelimitierung}
Eine Möglichkeit ist es, abzuwarten bis ein gewisser Anteil an Hintergrundjobs fertig ist.
Dies kann mittels einer Zählvariable und einer Pause gelöst werden.
Nachdem die Zählvariable einen gewissen Wert erreicht hat, soll das Programm 1 Minute und 30 Sekunden lange warten.
Somit sind alle Hintergrundjobs fertig und es können die nächsten gestartet werden.
\subsubsection{Unterschiedliche OIDs}
Bisher konnten wir für diese Problemstellung keine umsetzbare Lösung finden.
Wir haben uns entschieden, das Produkt vorerst für die Modellreihe Cisco 2960 anzuwenden.

\subsubsection{VLAN und MAC-Adressen}
Beim Scann ist eine Analyse der OID erforderlich, da nur Hardware MAC-Adressen gespeichert werden sollen.
Für VLAN MAC-Adressen ist die letzte Zahl der OID die VLAN-ID.
Diese kann maximal aus vier Stellen bestehen.
Bei den Hardware MAC-Adressen ist die letzte Zahl der OID fünfstellig.
Somit ist eine Filterung möglich.
Implementiert wurde es bisher noch nicht, ist aber im finalen Produkt vorgesehen.
Eine zusätzliche Erweiterung wäre, die Hardware MAC-Adressen und VLAN MAC-Adressen separat zu speichern.
