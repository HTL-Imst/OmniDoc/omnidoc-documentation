# Omnidoc Dokumentation
Dieses Repository enthält alle Dokumentationen für Omnidoc, welche mit Latex verfasst wurden.

## Arbeitsablauf
Soll die Dokumentation geändert werden, so sollten folgende Schritte beachtet werden:

1. **Repository aktualisieren** (z.B mit
`git pull`
)

2. **Latex Änderungen vornehmen**

3. **Commit Message schreiben**

4. **Hochladen** (z.B mit
`git push origin master`
)

## Latex Änderungen
Um Latex am besten zu versionieren sollte Jeder Satz in einer neuen Zeile beginnen.
Dies ist kein Problem, da der Latex Compiler erst ab zwei Absätzen eine neue Zeile anfängt.

Der Grund dafür ist, dass Git **bei Änderung eines Wortes** in einer Zeile **die ganze Zeile als Geändert markiert**.
Wenn nun mehrere Sätze in einer Zeile geschrieben worden sind, würden diese auch als Geändert markiert werden.

Optimal sollten unabhängige Textabschnitte (Titelblatt, Abstract, Kapitel, usw ... ) in **eigenen Dateien** verfasst werden, welche dann im Hauptdokument **inkludiert** werden.

### Aufteilung in mehrere Dokumente
In Latex kann der Befehl
`\input{Ordner/inp_document.tex}`
dazu verwendet werden, um die Datei *inp_document.tex* in das aktuelle Latex Dokument zu importieren.
Die Befehle in dem importierten Dokument werden dabei beibehalten.

Dieses Feature kann dazu verwendet werden, um größere Latex Dokumente aufzuteilen.

## Commit Messages
Um Änderungen der Dokumentation einheitlich nachverfolgen zu können sollten die Commit Messages so aufgebaut sein:

1. **Überschrift** - Soll in einigen Wörtern und in einer Zeile die Änderungen zusammenfassen. Hier sollte am Anfang einer dieser Prefixe stehen, damit die Commits einfacher zu unterscheiden sind:
    * **`[TASK]`** - Steht für einen Commit, der den Inhalt verändert. (Text)
    * **`[REFACTOR]`** - Steht für einen Commit, der die Formatierung, den Aufbau oder auch Fehler im Text verändert.
2. **Leere Zeile** - Durch eine Leere Zeile trennt man die Überschrift und den Inhalt.
3. **Beschreibung** - Listet die einzelnen Arbeitsschritte auf. Werden mehrere Schritte vereint, trennt man sie mit einem Minus. Als Beispiel:

        [TASK] initialisiere Repository

        - erstelle IT-Grundschutz Ordner
        - erstelle .gitignore Datei
        - Lösche Test Datei

 Als Beispiel der Output von
`git log --oneline`
**mit** und **ohne** einer leeren Zeile zwischen Überschrift und Beschreibung

>### **Mit leerer Zeile:**
>`052855c` **[TASK] initialisiere Repository**
>
>`b7ff419` **[REFACTOR] formatiere IT-Grundschutz**

Die erste Zeile der Commit Message wird von vielen Programmen als Überschrift verwendet (Gitlab, IDE's, ...).
Wird keine leere Zeile gemacht, wird die gesamte Commit Message angezeigt

>### **Ohne leerer Zeile:**
>`052855c` **[TASK] initialisiere Repository  - erstelle IT-Grundschutz Ordner  - erstelle .gitignore Datei  - Lösche Test Datei**
>
>`b7ff419` **[REFACTOR] formatiere IT-Grundschutz  - add image  - remove test content  - add PDF's to .gitignore file**

Für mehr Informationen bezüglich sauberer Commit Messages siehe
https://chris.beams.io/posts/git-commit/

## Branches & Tags
Für dieses Repository sind nicht mehrere Branches gedacht.
Tags können in Git dazu verwendet werden, um einen Commit als spezielle Version zu kennzeichnen.
In der Diplomarbeit kann dieses Feature dazu benutzt werden, um Abgaben zu kennzeichnen.

## .gitignore
Im versteckten `.gitignore` Files werden Dateien angegeben, die nicht versioniert werden sollen.
Die Angaben gelten immer für das Verzeichnis, in dem das `.gitignore` File liegt und für alle Unterverzeichnisse.
Momentan werden nur **Latex Dateien (.tex)**, **PDF Dateien (.pdf)** und **Bilder (.jpg, .png)** versioniert.
Der Grund dafür ist, dass Latex viele Dateien beim kompilieren erzeugt, welche sich ständig verändern.

Falls weitere Dateien hinzugefügt werden sollen, mich fragen oder selber ausprobieren. Die .gitignore Datei hat eine eigene Syntax.
